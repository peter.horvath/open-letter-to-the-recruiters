# Open letter to the recruiters

## Dear Recruiter,

If you see this message, I have already likely blocked you. Possibly I also reported you as spammer. Here I explain its reason.

Honestly, my heart bleeds doing so. Not because you would not deserve it. It bleeds because I see my previous myself in you, desperately fighting for my next job in Germany, getting the rejections again and again. With bullshit reasons or with no reason. I think, you are fighting for a cooperation with me (and then for "selling" me), as I fought for my job, some years ago. And any time if I just block one of you without an explanation, I feel myself being like these companies, rejecting my application. With a bullshit reason or with no reason. And I shame myself any time, if I block you. I feel that I am doing some really *evil*.

But, honestly, as I explain later, I still do no want to cooperate with you. The solution for this serious ethical problem is, *that I give you my true reason*. I think, this is something, what no company gives in this country to a rejected applicant; but I can do this to the rejected recruiters (and I reject all of you).

This is why I wrote this letter.

Not if you would be really interested on it. I already tried to explain the essence of this open letter to many of you. I hoped, maybe you can say something, what I can consider, and maybe it could have been a way for me to get better jobs. But not this happened. Practically all of you silently left. Sometimes you gave some "professional" bullshit and then left. *"Ich wünsche Ihnen alles gutes für die Zukunft"*, or so.

<div align="center">
!["This company wants you" - lied to the gull. "This candidate wants you" - lied to the company](src/gen/handcuff1.jpg "'This company wants you' - lied to the gull. 'This candidate wants you' - lied to the company")

"This company wants you" - lied to the gull. "This candidate wants you" - lied to the company
</div>

<br/>
<br/>

## However, you do not want any good for me in the future...

And that was just a lie. What you want, that is the recruiter fee. That is a huge disadvantage for me, if I apply with you for a job. There is no way to solve this conflict of interest.

This is what you want from me, and not any luck in the future. *"This is the free market"*, are you thinking likely now. And I say for that: o.k., but the same rules of the free market dictate me to not pay for something, what I could get also for free.

You are not nice people just helping me to get jobs. You want your recruiter fee. However, that would be still okay: I want the job, you want the fee. If I get the job, you get the fee. So our interests meet, so we fight together. I do not need to pay for you, only my employer pays. So you are really like some "helping angel" and it is a fortunate situation, that you exist. Roughly this is what I tought, in my infinite naivity. However, in the reality...

<div align="center">
![Shared the contact list of his previous employer with his friends](src/gen/handcuff2.jpg "Shared the contact list of his previous employer with his friends")

Shared the contact list of his previous employer with his friends
</div>

<br/>
<br/>

## I still need to pay a lot for working with recruiters, and the value of your service is nowhere to that.

Why? I explain.

You say, your service does not cost me anything. *This is not true, and you know it!* Your service costs about my 3 monthly salary - for my wannabe employer. This employer invests about 1-2 manmonth of work to find someone for an open position. He gets a lot of CVs, and selects from them one. Among these CVs, there are direct applications and also applications from recruiters.

But... your business model makes the companies interested to hire only direct applicants! They might interview also the people over recruiters, but if they consider their yearly budget, there will be a -15,000 EUR (or more), if they employ someone over recruiters, any time.

This is what a HR/Boss/Lead will see:

1. User1, skills (...), interview remarks (...), wants X1 EUR
2. User2, skills (...), interview remarks (...), wants X2 + 15,000 EUR
3. User3, skills (...), interview remarks (...), wants X3 + 15,000 EUR
4. User4, skills (...), interview remarks (...), wants X4 EUR
5. User5, skills (...), interview remarks (...), wants X5 + 15,000 EUR
...

The "+ 15,000" are the people coming over recruiters.

The companies will hire who they find the likely best. Typically, among the 10 candidates, the real decision will happen among 2 or 3. Again, that is never sure, simply that is how our brain works.

Being one of these top candidates, that is already hard. Who will actually win in his top 3.... that depends on nuances, for us it is more about luck. And I already have the disadvantage that my German is nothing comparable to a native speakers'. This is why your fee - what you get for the NOTHING - decreases the chances a lot.

And my stats say the same. *If I apply over a recruiter, my chance for an offer is about 1:60. If I apply directly, the chance is 1:10.* Beside the companies, also I have my notes, this is where the stat from.

Working with you does not increase the chance to get an offer, it hugely decreases it. In my case, to about a sixth.

If a company (boss) wants to employ me, he is doing it, because he can see a good chance for a 2-5 year long cooperation. Compared to my salary in this time, your fee is not a big prize. However, what a boss (company) needs to consider yet more strongly, *that is their yearly budget*. And yeah, compared to their yearly budget, that is already many. *It is enough to prefer a direct applicant*, hugely decreasing the chance of a successful application. To get a job offer over a recruiter, *I would need to be so much better than the second best applicant, what makes the boss (company) to select me even with your recruiter fee*. This is very hard - the second best is typically a direct applicant, so his hiring costs a company with 15,000 lesser.

So, for your service,

* I pay with lower salary,
* I pay with bad jobs,
* I pay with an increased chance of being unemployed again,
* I pay with a decrease of the chance of the so strongly wanted job offer to a sixth,
* and I pay with lesser amount of feedbacks/information about the rejection.

And what do I get back? Nothing. F..ly nothing.

<div align="center">
![In the reality, there was no job. He just collected contacts for the future](src/gen/handcuff3.jpg "In the reality, there was no job. He just collected contacts for the future")

In the reality, there was no job. He just collected contacts for the future
</div>

<br/>
<br/>

## And also your offers are crap

Why the heck you offer me, for example, a .net job, while it is not even mentioned anywhere in my CV or profile? I tried to avoid, intentionally, this technology, in my entire life. It is my personal taste. I am an oldboy linuxer, and I work with m$ things only, if unemployment is the other option. Other developers might have different taste, and I think, you are not better to match their profile to the job descriptions. Any time if I get a C#/.net offer, I could vomit. What crap AI directs your C#/.net searches into my CV? *Or are you contacting people completely randomly?* If yes, exactly what makes you different from a "nigerian prince"?

I also remember, as I was rejected from a low quality, underpaid PHP job, already in the recruiter-talk phase, because... because... so, the job description was this: "needed skills: php, frameworks". Probably the customer dictated to the recruiter, what he wants: a cheap PHP developer, it is better if he knows at least some PHP frameworks or so. So, recruiter noted: "customer X, 1 position, needed: PHP, frameworks". And then contacted me, or I contacted him (yes! I was really so stupid). First he asked, how good I am in PHP. I explained. Then he asked, and that "frameworks", do I know the "frameworks" tech? I tried to explain, there are various php frameworks, like wordpress, joomla and similars. He understood it so that I am bullshitting and in the reality I do not know "frameworks". And he canceled the talk.

I remember, as recently a recruiter guy, invested really a lot of effort to find a job for me. Beside that, I also applied directly for jobs I found. At the very end (20 applications, 15 rejections, 5 offers), 2 remained: one of the jobs this guy hunted, and the one what I found myself.

Unfortunately, the job what he found, was nowhere what I found. And I rejected the job what he found, and wasted him a lot, a lot, a lot of effort. I have seen a father of a small child, who now can't get a payment of 15,000 EUR, and he does not know, after how many months will he get his next fee. And I cheated him, I also blocked him (to decrease the chance of some revenge maneuver), *because I needed to care mine*. It had been much better if I had blocked him on the spot.

<div align="center">
![Self-declared himself as representant of the gull before all major company HRs of the city](src/gen/handcuff4.jpg "Self-declared himself as representant of the gull before all major company HRs of the city")

Self-declared himself as representant of the gull before all major company HRs of the city
</div>

<br/>
<br/>

## And that you pressurize me into crap jobs

I remember a recruiter, who tried to inject me into an interview as a team leader. I am not a boss and I won't be ever. He did not understand it. Why? The answer is obvious: because he only had that customer, and he only had me to talk with him. But at the time I was not sure, and he was convincing. Finally I lost some hours, he lost some hours and I rejected to get even the interview. Ultimately, I needed to block him (social media + spamfilter + phone), because he did not stop.

<div align="center">
![He is not a recruiter any more. He just buys, collects and sells candidate data](src/gen/handcuff5.jpg "He is not a recruiter any more. He just buys, collects and sells candidate data")

He is not a recruiter any more. He just buys, collects and sells candidate data
</div>

<br/>
<br/>

## And about your spamming/harassment

Can you imagine, how many trouble did you cause to the people, who you have called with (crap) job offers, *in worktime*? May I ask you, how would *your boss* likely react, if you would talk with others about job offers, *in worktime*?

You know, calling someone with job offer, *in worktime*, creates distrust if his bosses hear it? If you do not understand it, then you are only incompetent. If you understand, yet you call...

<div align="center">
![Wanted to attract candidates in their 20es. So he registered a fake account of a woman in her 20es](src/gen/handcuff6.jpg "Wanted to attract candidates in their 20es. So he registered a fake account of a woman in her 20es")

Wanted to attract candidates in their 20es. So he registered a fake account of a woman in her 20es
</div>

<br/>
<br/>

## And that you misuse the fear of the people from the unemployment

Honestly, if someone is looking for a job, is in a really, really bad position. His life is in threat. So, he fears. In this case, if an offer is coming, from any source, including recruiters, the last what we do, is that we block you. *But exactly this is what we should do*, and now you already know, why. If you have read this all... and you have some really, really convincing arguments, why my here written reasons do not happen in your case, you can drop me a mail to horvath.akos.peter at gmail.com. The only reason, why the linkedin/xing, and we, do not handle you as regular spammers, is this fear. But the fact is, that yes, your overwhelming majority is just a spammer, and your offer is not really more, as the nigerian prince wants to send me $2million.

<div align="center">
![Talked an hour with the gull about a fake job offer, in work time. His boss has heard it and fired the gull](src/gen/handcuff7.jpg "Talked an hour with the gull about a fake job offer, in work time. His boss has heard it and fired the gull")

Talked an hour with the gull about a fake job offer, in work time. His boss has heard it and fired the gull
</div>

<br/>
<br/>

## And yet another big NO-NO because your incompetence

I live here since 2012, and I have taken part maybe in about a 100 interviews. *2 of them were on English and the rest on German.* We could have talked also on English without any problem, but we did not.

And... Germany is special. You must understand, how the companies work, how the bosses think, what are the most typical fears and expectations of their customers, and how that affects the employers. *Believe me, it is f...ly not like the USA or a country of the former East Block. You need to learn Germany. And to learn Germany, you need to live in Germany. And you need to talk with them on German.* 

Note, if you are not a native English speaker, their English is probably better than yours. Yet they talk on German between each other, simply because it is a closed market where in most deals, both sides are native German speakers. Or it is at least important for them to emulate one (like me).

And now you appear with your "wonderful" offer.... on English. Do you know, what does it mean? That you have much lesser knowledge about this market, and much more far away from the good parts of this network, than I am! You can not help me, you would actually need my help! You have lost on the spot! I did not even read your phantastic "offer", and I already know, you are a looser, you are incompetent and you do not even deserve the time until I block you! Yes, because you wrote me a message on English! You have lost already after your first word!

<div align="center">
![Uses a fake account to learn, what type of job is wanted by the gull. Then later contacts the gull with a fake offer, from another fake account](src/gen/handcuff8.jpg "Uses a fake account to learn, what type of job is wanted by the gull. Then later contacts the gull with a fake offer, from another fake account")

Uses a fake account to learn, what type of job is wanted by the gull. Then later contacts the gull with a fake offer, from another fake account
</div>

<br/>
<br/>

## What to do

It would be beautiful if I would say something at the end. What could you do better, what could you do differently. It would be beautiful, but there is none. You can not do anything, except that you try to change your career. Maybe you could try to work in a model where companies pay for the applicants and not for the hirings, but if it would be possible, it would be long so.

Maybe you can try to find lesser learned gulls, and not me.

The sad truth is that the job market has 3 levels:

1. *Einsteigers* or *schwerbehindert* people (like me, with my bad German and not knowing the country). You could help them a lot - *except that you do not*. It is not your interest. You did not help me, except by what also I could have done (spamming companies with my bad CV, what you did not even try to understand).
2. Medium-level people, like me. Fact is that you do not have anything what I would need. The little what you could help, that you never do.
3. Top guys, CEOs or so. If someone controls a $100M/yr company, then it might worth to hunt the whole U.S., investing tens of man-years to find him. I am obiously very far from this weight group and I won't be there ever.

I would suggest to focus to (1) and (3) - the problem is that you can not get too much from (1), and there is only a little amount of need for (3).

Yours sincerely,

Peter Horvath
